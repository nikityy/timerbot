# Aleks Ivanchenko's Deploy

## Prerequisites

Install [Ansible](https://www.ansible.com). Copy `id_rsa` and `id_rsa.pub` keys to `~/.ssh`. Execute following command:

    ssh-add ~/.ssh/id_rsa

And enter the passcode. You're ready.

## Deploying Bot

Run the following command:

    ansible-playbook -i <INVENTORY_FILE> deploy_bot.yml
