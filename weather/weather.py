# -*- coding: UTF-8 -*-
import requests


class Weather(object):
    CONDITIONS = {
        'clear': 'ясно',
        'partly-cloudy': 'малооблачно',
        'cloudy': 'облачно с прояснениями',
        'overcast': 'пасмурно',
        'partly-cloudy-and-light-rain': 'небольшой дождь',
        'partly-cloudy-and-rain': 'дождь',
        'overcast-and-rain': 'сильный дождь',
        'overcast-thunderstorms-with-rain': 'сильный дождь, гроза',
        'cloudy-and-light-rain': 'небольшой дождь',
        'overcast-and-light-rain': 'небольшой дождь',
        'cloudy-and-rain': 'дождь',
        'overcast-and-wet-snow': 'дождь со снегом',
        'partly-cloudy-and-light-snow': 'небольшой снег',
        'partly-cloudy-and-snow': 'снег',
        'overcast-and-snow': 'снегопад',
        'cloudy-and-light-snow': 'небольшой снег',
        'overcast-and-light-snow': 'небольшой снег',
        'cloudy-and-snow': 'снег'
    }

    def __init__(self, token):
        self.token = token

    def get_forecast(self, **kwargs):
        response = self.get(rate='forecast', **kwargs)

        fact = response['fact']
        string_format = 'Сейчас {temp}ºС, {cond}, скорость ветра: {wind_speed} м/c.'

        return string_format.format(temp=int(fact['temp']),
                                    cond=Weather.CONDITIONS[fact['condition']],
                                    wind_speed=int(fact['wind_speed']))

    def get(self, rate, **kwargs):
        url = "https://api.weather.yandex.ru/v1/{}".format(rate)
        headers = {'X-Yandex-API-Key': self.token}
        params = kwargs
        response = requests.get(url,
                                headers=headers,
                                params=params)

        return response.json()
