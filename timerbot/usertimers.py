from .models.usertimer import UserTimer

class UserTimers():
    def get(self, user_id, session):
        timer = session.query(UserTimer).one_or_none()

        if timer:
            return timer
        else:
            timer = UserTimer()
            timer.id = user_id

            session.add(timer)

            return timer
