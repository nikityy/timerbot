from datetime import datetime
from telegram import Bot
from weather import Weather

from .config import token, yandex_weather_token
from .models.usertimer import UserTimer
from .session import session_context

class Poll(object):
    def __init__(self, telegram_bot, weather_client):
        self.weather = weather_client
        self.bot = telegram_bot

    def check(self):
        now = datetime.now()

        with session_context() as session:
            items = session.query(UserTimer)\
                           .filter_by(is_running=True)\
                           .filter(UserTimer.next_tick < now)\
                           .all()

            for item in items:
                self.notify_user(item.id)
                item.schedule_next()

            session.add_all(items)

    def notify_user(self, user_id):
        try:
            self.send_forecast(user_id)
        except:
            self.send_user_message(user_id, 'Подъем.')

    def send_forecast(self, user_id):
        longitude = 27.561481
        latitude = 53.902496
        forecast = self.weather.get_forecast(lon=longitude, lat=latitude)

        self.send_user_message(user_id, forecast)

    def send_user_message(self, chat_id, message):
        self.bot.send_message(chat_id, message)

if __name__ == '__main__':
    bot = Bot(token=token)
    weather = Weather(token=yandex_weather_token)
    poll = Poll(telegram_bot=bot, weather_client=weather)

    poll.check()
