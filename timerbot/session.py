from contextlib import contextmanager
from sqlalchemy.orm import sessionmaker
from .engine import engine

Session = sessionmaker()
Session.configure(bind=engine)

@contextmanager
def session_context(auto_commit=True):
    session = Session()
    try:
        yield session
    except Exception as err:
        session.rollback()
        raise err
    finally:
        if auto_commit:
            session.commit()
        session.close()
