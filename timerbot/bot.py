import logging

from .config import token
from .engine import engine
from .models.base import Base
from .timerbot import Timerbot

if __name__ == '__main__':
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO
    )

    Base.metadata.create_all(engine)

    timerbot = Timerbot(token=token)
    timerbot.start()
