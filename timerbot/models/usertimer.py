from datetime import datetime
from croniter import croniter
from sqlalchemy import Boolean, Column, DateTime, Integer, String
from .base import Base


class UserTimer(Base):
    __tablename__ = 'usertimers'

    id = Column(Integer, primary_key=True)
    next_tick = Column(DateTime)
    schedule = Column(String, default="*/30 * * * *")
    is_running = Column(Boolean, default=False)

    def __init__(self):
        self.schedule = "*/30 * * * *"

    def start(self):
        self.schedule_next()
        self.is_running = True

    def schedule_next(self):
        self.next_tick = self.get_next_tick()

    def get_next_tick(self):
        now = datetime.now()
        cron = croniter(self.schedule, now)

        return cron.get_next(datetime)

    def setup(self, schedule):
        self.schedule = schedule
        self.schedule_next()

    def left_minutes(self):
        next_tick = self.get_next_tick()
        delta = next_tick - datetime.now()
        return int(delta.total_seconds() // 60) + 1

    def stop(self):
        self.is_running = False
        self.next_tick = None
