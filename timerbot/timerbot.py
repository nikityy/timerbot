from contextlib import contextmanager
from telegram.ext import CommandHandler, Updater

from .session import session_context
from .usertimers import UserTimers


def dbcontext(func):
    def inner(self, *args, **kwargs):
        with session_context() as session:
            return func(self, *args, session=session, **kwargs)
    return inner


class Timerbot(object):
    """
    Connects to Telegram API and waits for user messages. If user send one,
    dispatches it and makes changes to timer schedule.
    """

    def __init__(self, token):
        self.timers = UserTimers()
        self.updater = Updater(token=token)
        self.dispatcher = self.updater.dispatcher

        start_handler = CommandHandler('start', self.on_start_message)
        self.dispatcher.add_handler(start_handler)

        stop_handler = CommandHandler('stop', self.on_stop_message)
        self.dispatcher.add_handler(stop_handler)

        set_timeout_handler = CommandHandler('setup', self.on_setup_message)
        self.dispatcher.add_handler(set_timeout_handler)

        status_handler = CommandHandler('status', self.on_status_message)
        self.dispatcher.add_handler(status_handler)

    def start(self):
        self.updater.start_polling()

    def on_start_message(self, bot, update):
        """
        Handles user's 'start' command. Creates a new timer if not exists
        """

        chat_id = update.message.chat_id
        with self.get_timer(chat_id) as timer:
            if timer.is_running:
                bot.send_message(chat_id, 'But you\'ve already scheduled a timer')
            else:
                timer.start()
                left_minutes = timer.left_minutes()
                bot.send_message(chat_id, 'OK. Next timeout in {} minutes'.format(left_minutes))

    def on_setup_message(self, bot, update):
        """
        Handles user's 'setTimeout' command. Changed timer's timeout
        """

        chat_id = update.message.chat_id

        with self.get_timer(chat_id) as timer:
            text = update.message.text
            data = text.split(' ', 1)

            if len(data) == 2:
                _, schedule = data
                timer.setup(schedule)
                bot.send_message(chat_id, 'OK, the new schedule has been applied')
            else:
                bot.send_message(chat_id, 'Pass CRON schedule as the second argument')

    def on_status_message(self, bot, update):
        """
        Handles user's 'status' command. Prints if any timer has been set
        """

        chat_id = update.message.chat_id

        with self.get_timer(chat_id) as timer:
            message = 'Your schedule: {}'.format(timer.schedule)

            if timer.is_running:
                left_minutes = timer.left_minutes()
                message += '\nNext timeout in {} minutes'.format(left_minutes)

            bot.send_message(chat_id, message)

    def on_stop_message(self, bot, update):
        """
        Handles user's 'stop' command. Removes timer if it exists
        """

        chat_id = update.message.chat_id
        with self.get_timer(chat_id) as timer:
            if timer.is_running:
                timer.stop()
                bot.send_message(chat_id, 'OK, I\'ll not bother you anymore')
            else:
                bot.send_message(chat_id, 'But you\'ve not scheduled one')

    @contextmanager
    @dbcontext
    def get_timer(self, chat_id, session):
        chat_id = str(chat_id)
        timer = self.timers.get(chat_id, session)

        yield timer

        session.add(timer)
        session.commit()
